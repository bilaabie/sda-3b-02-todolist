from os import times
import datetime as dt
from tkinter import *
from  tkinter import ttk
from tkinter import font



class Node:
    def __init__(self, data, time):
        self.time = time
        self.data = data
        self.ref = None

class LinkedList:
    def __init__(self):
        self.head = None

    def print_LL(self):
        for item in todo.get_children():
            todo.delete(item)
        if self.head is None:
            print("Linked list is empty")
        else:
            n = self.head
            b = 1
            while n is not None:
                a = n.data
                tm = n.time
                n = n.ref
                todo.insert('', 'end', b-1 ,values=(b,a,tm))
                b += 1
        todo.pack()
               
        
    
    def add_begin(self,data,time):
        new_node = Node(data,time)
        new_node.ref = self.head
        self.head = new_node


    def add_end(self,data,time):
        new_node = Node(data,time)
        if self.head is None:
            self.head = new_node
        else:
            n = self.head
            while n.ref is not None:
                n = n.ref
            n.ref = new_node
            
    def add_after(self,data,time,x):
        n = self.head
        r = int(x) - 1
        while r > 0:
            if n.ref == None:
                return 4
            n = n.ref
            r = r-1
        new_node = Node(data,time)
        new_node.ref = n.ref
        n.ref = new_node

    def remove_list(self, x):
        if self.head is None:
                print("To Do List is Empty")
               
        if x == self.head.data:
            self.head = self.head.ref
            return

        n = self.head
        while n.ref is not None:
            if x == n.ref.data:
                break
            n = n.ref
        if n.ref is None:
            print("Data tidak ada")
        else:
            n.ref = n.ref.ref

    def edit(self, z, p):
        if self.head is None:
                print("To Do List is Empty")

        print(f"reached p = {p}, z={z}")

        if z == self.head.data:
            self.head.data = p
            return

        print(f"reached p = {p}, z={z}")
        f = self.head
        while f.ref is not None:
            if f.data == z:
                break
            else: 
                f = f.ref

        f.data = p



LL1 = LinkedList()

ws  = Tk()
ws.title('To Do List')
ws.geometry('500x500')
ws['bg'] = '#F6F4FC'


canvas1 = Frame(ws, bg="#F6F4FC")
canvas1.pack(side=TOP)
canvas1.grid_anchor("center")

label1 = Label(canvas1, text='TODAY TO DO LIST')
label1.config(font=('poppins', 14, 'bold'), bg=("#F6F4FC"))
label1.pack()

today = dt.datetime.now()


label2 = Label(canvas1, text='{:%d - %B - %Y}'.format(today))
label2.config(font=('poppins', 10), bg=("#F6F4FC"))
label2.pack()




activity = Frame(canvas1, bg="#F6F4FC")
activity.pack(side=LEFT,ipadx=5, pady=10)

labeld = Label(activity, text="Activity", bg="#F6F4FC")
labeld.pack()
data = Entry (activity)
data.pack()



times = Frame(canvas1, bg="#F6F4FC")
times.pack(side=RIGHT,ipadx=5, pady=10)
labelt = Label(times, text="Time", bg="#F6F4FC")
labelt.pack()
time = Entry (times)
time.pack()




todo_frame = Frame(ws)

todo = ttk.Treeview(todo_frame, show='headings')



def btn1():
    datas = data.get()
    tm = time.get()
    LL1.add_begin(datas,tm)
    LL1.print_LL()

def btn3():
    datas = data.get()
    tm = time.get()
    LL1.add_end(datas,tm)
    LL1.print_LL()

def btn2():
    global pop
    pop = Toplevel(ws)
    pop.title("Add After")
    pop.geometry("150x95")
    pop.config(bg="#F6F4FC")
    
    pop_label = Label(pop, text="Add After no. : ", bg="#F6F4FC")
    pop_label.pack(ipady=10)

    af = Entry(pop)
    af.pack()

    def addAfter():
        # pop.destroy()
        tm = time.get()
        datas = data.get()
        after = af.get()
        LL1.add_after(datas,tm,after)
        LL1.print_LL()

    btns = Frame(pop)
    btns.pack(pady=8)
    addbtn = Button(btns, text="Add", command= addAfter)
    addbtn.pack()
    
def remove():
    selected = todo.focus()
    values = todo.item(selected,'values')
    # labs = Label(rmv, text=values[1])
    # labs.pack()
    # todo.delete(selected_item)
    LL1.remove_list(values[1])
    # datas = data.get()
    # LL1.remove_list(datas)
    LL1.print_LL()

def edit():
    selected = todo.focus()
    values = todo.item(selected,'values')
    global pop
    pop = Toplevel(ws)
    pop.title("edit")
    pop.geometry("150x95")
    pop.config(bg="#F6F4FC")
    
    pop_label = Label(pop, text="edit : ", bg="#F6F4FC")
    pop_label.pack(ipady=10)

    ed = Entry(pop)
    ed.pack()

    def edits():
        # pop.destroy()
        vi = ed.get()
        LL1.edit(values[1], vi)
        LL1.print_LL()

    btns = Frame(pop)
    btns.pack(pady=8)
    addbtn = Button(btns, text="confirm", command= edits)
    addbtn.pack()

# ==============button=================

btn = Frame(ws, bg="#F6F4FC")
btn.pack(side=TOP, ipady=10, pady=5)

button1 = Button(btn, text="Add First", command = btn1)
button1.pack(side=LEFT)

button2 = Button(btn, text="Add After", command=btn2)
button2.pack(side=LEFT, padx=8, ipadx=8)

button3 = Button(btn, text="Add End", command= btn3)
button3.pack(side=LEFT, ipadx=8)

button4 = Button(btn, text="edit", command= edit, bg='#4F3B8F', fg='white')
button4.pack(side=RIGHT, padx=8, ipadx=8)

rmv = Frame(ws, bg="#F6F4FC")
rmvBtn = Button(rmv, text="Remove", command=remove, bg='red', fg='white')


# ==============table=================

todo_frame.pack()

todo['columns'] = ('no', 'act', 'time')

todo.column("#0", width=0,  stretch=NO)
todo.column("no",anchor=CENTER, width=50)
todo.column("act",anchor=CENTER,width=150)
todo.column("time",anchor=CENTER,width=80)



todo.heading("no",text="No",anchor=CENTER)
todo.heading("act",text="Activities",anchor=CENTER)
todo.heading("time",text="Time",anchor=CENTER)



        



todo.pack()
rmv.pack()
rmvBtn.pack(pady=10)

ws.mainloop()