from prettytable import PrettyTable
    
t = PrettyTable()


t.field_names = ["To Do List"]

class Node:
    def __init__(self, data):
        self.data = data
        self.ref = None

class LinkedList:
    def __init__(self):
        self.head = None

    def print_LL(self):
        print("\n", 10*"=", "INPUT TO DO LIST", 10*"=", "\n")
        if self.head is None:
            print("Linked list is empty")
        else:
            n = self.head
            while n is not None:
                a = n.data
                n = n.ref
                t.add_row([a])
            print(t)
        pilih = input("Kembali ke Menu Utama ? (y/n) : ")
        if pilih == 'y':
            main()

    
    def add_begin(self,data):
        new_node = Node(data)
        new_node.ref = self.head
        self.head = new_node
        

    def add_end(self,data):
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
        else:
            n = self.head
            while n.ref is not None:
                n = n.ref
            n.ref = new_node
            
    def add_after(self,data,x):
        n = self.head
        while n is not None:
            if x==n.data:
                break
            n = n.ref
        if n is None:
            print("node is not present in LL")
        else:
            new_node = Node(data)
            new_node.ref = n.ref
            n.ref = new_node
            

    def delete_by_value(self):
        print("\n", 10*"=", "INPUT TO DO LIST", 10*"=", "\n")
        print(t)
        i = 0
        while i == 0:
            x = input("Data yang mau dihapus : ")
            if self.head is None:
                print("To Do List is Empty")
                break
            if x == self.head.data:
                self.head = self.head.ref
                break
            n = self.head
            while n.ref is not None:
                if x==n.ref.data:
                    break
                n = n.ref
            if n.ref is None:
                print("Data tidak ada")
                break
            else:
                n.ref = n.ref.ref

            again = input("\nHapus lagi? (y/n) : ")
            if again == 'n':
                main()
            



def inputToDo():
    i = 0
    print("\n", 10*"=", "INPUT TO DO LIST", 10*"=")
    print("\n1. Tambahkan list di awal")
    print("2. Tambahkan list di akhir")
    print("3. Sisipkan List")
    while i == 0:
        pilih = int(input("\nPilih : "))
        if pilih == 1:
            addFirst = input("Masukkan kegiatan : ")
            LL1.add_begin(addFirst)
        elif pilih == 2:    
            addEnd = input("Masukkan kegiatan : ")
            LL1.add_end(addEnd)
        elif pilih == 3:
            addAfter = input("Masukkan kegiatan : ")
            urutan = input("Disisipkan setelah : ")
            LL1.add_after(addAfter,urutan)
        again = input("\nTambahkan lagi? (y/n) : ")
        if again == 'n':
            main()


def main():
    print("\n",10*"=","YOUR TO DO LIST", 10*"=")
    print("1. Input To Do List")
    print("2. View To Do List")
    print("3. Remove To Do List")
    pilih = int(input("Pilih Menu : "))
    if pilih == 1:
        inputToDo()
    elif pilih == 2:
        t.clear_rows()
        LL1.print_LL()
        # a = LL1.print_LL()
        #t.add_row([a])
        #print(t)
    elif pilih == 3:
        LL1.delete_by_value()


LL1 = LinkedList()
main()
#LL1.print_LL()
#x.add_column(title[0], a)
#print(x)